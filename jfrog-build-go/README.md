## Objective

This job will allow you to ...

## How to use it

!!! info
    You can use this job with ...

1. Ensure that your project has a pre-made environment file such as `.env.testing` in example, which contains the variables that will be used by the project.
2. Copy/paste job URL in `include` list of your `.gitlab-ci.yml` (see the [quick use](https://docs.r2devops.io/get-started/use-templates/#use-a-template)). You can specify [a fixed version](https://docs.r2devops.io/get-started/use-templates/#versioning) instead of `latest`.
3. If you need to customize the job (stage, variables, ...) 👉 check the [jobs
   customization](https://docs.r2devops.io/get-started/use-templates/#job-templates-customization)
4. Well done, your job is ready to work ! 😀


## Variables

!!! info
    If you have setup Gitlab's CI/CD variables, they will be used instead of the ones defined in `.env`, just make sure to name them exactly the same.

| Name | Default | Description |
| ---- | ------- | --------------- |
| `Var1` | `.` | Path to the directory containing environment variables |
| `Var2` | `.env.testing` | Name of the environment variables file to use |


## Artifacts



## Author

