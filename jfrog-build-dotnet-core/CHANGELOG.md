# Changelog
All notable changes to this job will be documented in this file.

## [0.3.1] - 2023-03-03
* Fix default stage

## [0.3.0] - 2023-03-03
* Add default stage
* Rename job name

## [0.2.0] - 2023-03-02
* Change script name

## [0.1.1] - 2023-03-02
* Update setup-ci

## [0.1.0] - 2023-03-01
* Initial version
