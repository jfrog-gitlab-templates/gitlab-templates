# Changelog
All notable changes to this job will be documented in this file.

## [0.1.2] - 2023-03-01
* Rename job

## [0.1.1] - 2023-03-01
* Add documentation

## [0.1.0] - 2023-03-01
* Initial version
